package com.facebooknetwork;

import java.util.ArrayList;

/**
 * This is user class used to define how to login into a login page in the 
 * social network application
 * @author vchukka
 *
 */
public class User {
	//String user_Id;  //representation of user id;
	String name;     //representation of name;
	String email;    //repersentation of email;
	String password; //repersentation of password;
	//private ArrayList<Address> address;   	// rating array contains Rating objects

	
	/**
	 * this is basic constructor
	 * @param user_Id
	 * @param name
	 * @param email
	 * @throws Exception 
	 */
	public User(String name,String email,String password) throws Exception {
		if(name.trim().length() < 2 || name.trim().length() > 8) {
			// validation of name failure
			throw new Exception("Name is too short.");
		}
		else if(password.contains("$") || email.contains("@")) {
			// validation of description 
			throw new Exception("entered username or email  ");
		}
		else if(password.length() < 2 || password.length() > 8)
				{
			// validation of password failure
			throw new Exception("password failure");
				}
		 else {
		//this.user_Id  = user_Id;
		this.name     = name;
		this.email    = email;
		this.password = password;
	}
	}
	
	//to string method
	@Override
	public String toString() {
		String userreturn = "this is user details\n";
		//userreturn+="user_Id    :"+this.user_Id+"\n";
		userreturn+="username   :"+this.name+"\n";
		userreturn+="useremail  :"+this.email+"\n";
		userreturn+="password   :"+this.password+"\n";
		return userreturn;	    
}
}
